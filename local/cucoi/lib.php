<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/totara/customfield/fieldlib.php';

function local_cucoi_import(&$courses, $map) {
	
    global $DB;

    if (empty($courses)) {
        return;
    }

    $course_chunks = array_chunk($courses, 50);
    foreach ($course_chunks as $course_batch) {
        foreach ($course_batch as $coursedata) {

            $course = local_cucoi_render_course($coursedata, $map);
            if (empty($course->category)) {
                mtrace('Error: course "'.$course->shortname.'" does not have category, skipping.');
                mtrace(print_r($course, true));
                continue;
            }

            // Fetch the course data, or create if not exists.
            // Key is the short name.
            $c = $DB->get_record('course', array('shortname' => $course->shortname));
            if (!$c) {
                $c = create_course($course);
                if ($c) {
                    mtrace('Course "'.$course->shortname.'" created.');
                } else {
                    mtrace('Error: course "'.$course->shortname.'" could not be created, skipping.');
                    continue;
                }
            }

            $course->id = $c->id;

            update_course($course);
            
            // Section data
            course_create_sections_if_missing($course->id, array(0));
            if (!empty($course->section0_name)) {
                $sql = "UPDATE {course_sections} SET
                        name = ?,
                        summary = ?
                    WHERE course = ?
                        AND section = 0";
                $DB->execute($sql, array(
                    $course->section0_name,
                    (!empty($course->section0_summary) ? $course->section0_summary : NULL),
                    $course->id,
                ));
            }
            
            local_cucoi_course_save_customdata($course);

            mtrace('Course "'.$course->shortname.'" ('. $course->id .') updated.');
        }
    }
	
}

function local_cucoi_course_save_customdata($course) {
	
    global $CFG, $DB;

    $customfields = $DB->get_records_select('course_info_field', null, array());

    foreach ($customfields as $cfield) {

        $field = 'customfield_'.$cfield->shortname;
        if (empty($course->{$field})) {
            continue;
        }

        $itemnew = new stdClass();
        $itemnew->id = $course->id;
        $itemnew->{$field} = $course->{$field};

        require_once($CFG->dirroot.'/totara/customfield/field/'.$cfield->datatype.'/field.class.php');
        $newfield = 'customfield_'.$cfield->datatype;
        $formfield = new $newfield($cfield->id, $itemnew, 'course', 'course');

        local_cucoi_render_customfield($itemnew, $field, $course, $formfield);

    }

    customfield_save_data($course, 'course', 'course');
}

function local_cucoi_render_customfield(&$itemnew, $field, &$course, $formfield) {
	
    // FIXME Some elements are of "multichoice" type - we can't know which
    $multichoice_fields = array(
        'customfield_locationoptions',
    );
    if (in_array($field, $multichoice_fields)) {
        $selections = explode(',', $course->{$field});
        $itemnew->{$field} = array();
        foreach ($formfield->options as $index => $option) {
            if (in_array($option['option'], $selections)) {
                $itemnew->{$field}[] = $index;
            }
        }
        $course->{$field} = $itemnew->{$field};
    }

    // FIXME Some elements are of "menu" type - we can't know which
    $menu_fields = array(
        'customfield_accreditationstatus',
        'customfield_modulestatus',
        'customfield_coursedelivery',
        'customfield_trainingprovidername',
    );
    //echo $field.' - '.$cfield->datatype."\n";
    if (in_array($field, $menu_fields)) {
        if ((int) $course->{$field} == 0) {
            // Convert string value to a numeric index.
            $course->{$field} = array_search($course->{$field}, $formfield->options);
        }
    }

    // FIXME Some elements are of "textarea" type - we can't know which
    $textarea_fields = array(
        'customfield_prerequesites',
        'customfield_learningoutcomes',
        'customfield_costnotes',
        'customfield_modulenotes',
    );
    if (in_array($field, $textarea_fields)) {
        $course->{$field.'_editor'}['text'] = $course->{$field};
        $course->{$field.'_editor'}['format'] = FORMAT_HTML;
    }

}

function local_cucoi_render_course($coursedata, $map) {
	
    global $TOTARA_COURSE_TYPES;
    
    $data = str_getcsv($coursedata);
    $course = new stdClass();

    $startdate = null;
    if (!empty($map['startdate']) && !empty($data[$map['startdate']])) {
        $dt = DateTime::createFromFormat('d/m/Y', $data[$map['startdate']]);
        if ($dt) {
            $startdate = $dt->getTimestamp();
        }
    }

    foreach ($map as $field => $index) {
        if (!empty($data[$index])) {
            $course->{$field} = $data[$index];
        }
    }

    if (!empty($startdate)) {
        $course->startdate = $startdate;
    }

    $course->coursetype = null;
    if (!empty($map['coursetype']) && !empty($data[$map['coursetype']])) {
        foreach ($TOTARA_COURSE_TYPES as $label => $id) {
            if (strtolower($label) == strtolower($data[$map['coursetype']])) {
                $course->coursetype = $id;
                break;
            }
        }
    }
	
    return $course;
}

function local_cucoi_build_data_map($header) {
    $data = str_getcsv($header);
    $map = array();
    foreach ($data as $i => $value) {
        if (!preg_match('/^[a-z0-9_]+$/', $value)) {
            continue;
        }
        $map[$value] = $i;
    }
    return $map;
}

function local_cucoi_load($filepath) {
	
    if (!file_exists($filepath)) {
        mtrace("File not found.");
        return false;
    }
    
    return explode("\r\n", file_get_contents($filepath));
}
