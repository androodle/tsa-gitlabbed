<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->libdir.'/cronlib.php');
require_once('lib.php');

$csvpath = 'courses.csv'; // TODO switch to upload form

mtrace('Custom course import begins...');
$data = local_cucoi_load($csvpath);
if (!$data) {
	mtrace('Custom course import stopped.');
	exit;
}

mtrace('CSV loaded, parsing...');
$header = array_shift($data);
$map = local_cucoi_build_data_map($header);
if (!$map) {
	mtrace('Custom course import stopped.');
	exit;
}

mtrace('Data prepared, importing now...');
local_cucoi_import($data, $map);

mtrace('Custom course import ended.');
