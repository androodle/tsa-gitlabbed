<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

$string['allow_guest_access'] = 'Allow guest access';
$string['allow_guest_access_explanation'] = 'Allow users who are not logged in to view the catalogue entries.';
$string['androgogic_catalogue:view'] = 'View and search objects within the Androgogic Catalogue block';
$string['androgogic_catalogue:edit'] = 'Edit objects within the Androgogic Catalogue block';
$string['androgogic_catalogue:delete'] = 'Delete objects within the Androgogic Catalogue block';
$string['calendar'] = 'Calendar';
$string['capability_element'] = 'Capability Element';
$string['catalogue'] = 'Catalogue';
$string['catalogue_comp_framework'] = 'Competency framework';
$string['catalogue_comp_framework_explanation'] = 'If selected, Catalogue will only display results from this framework.';
$string['catalogue_entry'] = 'Catalogue Entry';
$string['catalogue_entry_delete'] = 'Delete Catalogue Entry';
$string['catalogue_entry_edit'] = 'Edit Catalogue Entry';
$string['catalogue_entry_new'] = 'Add a new Catalogue Entry';
$string['catalogue_entry_plural'] = 'Catalogue Entries';
$string['catalogue_entry_search'] = 'Catalogue Entries';
$string['catalogue_entry_search_instructions'] = 'Search by Name or Description';
$string['catalogue_view_mode'] = 'Display entries as';
$string['catalogue_view_mode_explanation'] = 'Controls the default presentation of the calendar.';
$string['cohort'] = 'Audience';
$string['competency'] = 'Competency';
$string['course'] = 'Course';
$string['created_by'] = 'Created By';
$string['datasubmitted'] = 'Data saved successfully.';
$string['date_created'] = 'Date Created';
$string['date_modified'] = 'Date Modified';
$string['description'] = 'Description';
$string['end_date'] = 'End Date';
$string['enddate'] = 'and';
$string['entriesfound'] = '{$a} results found.';
$string['entrynotfound'] = 'Catalogue entry not found.';
$string['filtersettings'] = 'Filtering settings';
$string['hide_search_form'] = 'Hide search form';
$string['hide_search_form_explanation'] = 'Hides the search form from the catalogue entries page.<br><b>Note</b> that the search parameters passed via URL will still work.';
$string['id'] = 'ID';
$string['idwrong'] = 'ID is invalid or empty.';
$string['itemdeleted'] = 'The item has been deleted';
$string['location'] = 'Location';
$string['location_delete'] = 'Delete Location';
$string['location_edit'] = 'Edit Location';
$string['location_name'] = 'Location name';
$string['location_new'] = 'Add a new Location';
$string['location_plural'] = 'Locations';
$string['location_search'] = 'Locations';
$string['location_search_instructions'] = 'Search by name';
$string['locationnotfound'] = 'Location not found.';
$string['modified_by'] = 'Modified By';
$string['name'] = 'Name';
$string['organisation'] = 'Organisation';
$string['noresults'] = 'There were no results from your search.';
$string['pluginname'] = 'Androgogic Catalogue';
$string['plugintitle'] = 'Androgogic Catalogue';
$string['position'] = 'Position';
$string['program'] = 'Program';
$string['public'] = 'Public';
$string['standard'] = 'Standard';
$string['startdate'] = 'Course or program starts between: ';
$string['viewascalendar'] = 'View as a calendar';
$string['viewaslist'] = 'View as a list';
$string['visibilitysettings'] = 'Visibility settings';
