<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die(); 

if ($ADMIN->fulltree) {
    
    $settings->add(new admin_setting_configcheckbox(
        'catalogue_allow_guest_access', 
        get_string('allow_guest_access', 'block_androgogic_catalogue'),
        get_string('allow_guest_access_explanation', 'block_androgogic_catalogue'), 
        0
    ));
    
    $settings->add(new admin_setting_configcheckbox(
        'catalogue_hide_search_form', 
        get_string('hide_search_form', 'block_androgogic_catalogue'),
        get_string('hide_search_form_explanation', 'block_androgogic_catalogue'), 
        0
    ));
    
    $settings->add(new admin_setting_configselect(
        'catalogue_view_mode', 
        get_string('catalogue_view_mode', 'block_androgogic_catalogue'),
        get_string('catalogue_view_mode_explanation', 'block_androgogic_catalogue'), 
        'standard',
        array(
            'standard' => get_string('standard', 'block_androgogic_catalogue'),
            'calendar' => get_string('calendar', 'block_androgogic_catalogue'),
        )
    ));
    
    $frameworks = \block_androgogic_catalogue\catalogue::get_comp_frameworks();
    array_unshift($frameworks, ''); // add empty value
    
    $settings->add(new admin_setting_configselect(
        'catalogue_comp_framework', 
        get_string('catalogue_comp_framework', 'block_androgogic_catalogue'),
        get_string('catalogue_comp_framework_explanation', 'block_androgogic_catalogue'), 
        0,
        $frameworks
    ));
    
}
