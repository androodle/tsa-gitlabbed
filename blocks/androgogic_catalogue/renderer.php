<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

class block_androgogic_catalogue_renderer extends plugin_renderer_base 
{ 
    protected $total_count = 0;
    protected $page_num = 0;
    protected $perpage = 20;
    
    public function show_results($mode, &$results, $total_count, $page) 
    {
        $this->total_count = $total_count;
        $this->page_num = $page;
        
        switch ($mode) {
            case 'calendar':
                $out = $this->display_calendar($results);
                break;
            case 'standard':
            default:
                $out = $this->display_standard($results);
        }
        
        return $out;
    }
    
    protected function show_pagebar()
    {
        $pagingbar = new paging_bar($this->total_count, $this->page_num, $this->perpage, $this->page->url);
        $pagingbar->pagevar = 'page';
        echo $this->output->render($pagingbar);
    }
    
    protected function display_standard(&$results)
    {
        ob_start();
        
        $this->show_pagebar();
        
        foreach ($results as $result) 
        {
            $entry = new \block_androgogic_catalogue\entry($result);
            if (!$entry->is_available_for_user()) {
                continue;
            }

            // Display caption.
            $links = array();
            if (has_capability('block/androgogic_catalogue:edit', $this->page->context)) {
                $links[] = html_writer::tag('a', get_string('edit'), array(
                    'href' => new moodle_url('edit.php', array('id' => $entry->id))
                ));
            }
            if (has_capability('block/androgogic_catalogue:delete', $this->page->context)) {
                $links[] = \html_writer::tag('a', get_string('delete'), array(
                    'href' => new moodle_url('index.php', array('delete' => $entry->id)),
                    'onclick' => "javascript:return confirm('".get_string('areyousure')."')"
                ));
            }

            echo html_writer::start_tag('div', array('class' => 'catalogue-entry'));
            
            echo html_writer::start_tag('h3');
            echo $entry->name;
            echo html_writer::tag('div', implode(' &middot; ', $links), array('class' => 'catalogue-caption-links'));
            echo html_writer::end_tag('h3');

            $entry->description = file_rewrite_pluginfile_urls(
                $entry->description, 
                'pluginfile.php', 
                $this->page->context->id, 
                'block_androgogic_catalogue',  
                'catalogue_entry_description', 
                $entry->id
            );
            echo format_text($entry->description, FORMAT_HTML);

            // Display courses.
            $courses = $entry->get_courses();
            if (!empty($courses)) {
                $links = array();
                foreach ($courses as $course) {
                    $links[] = html_writer::tag('a', $course->fullname, array('href' => $course->url));
                }
                echo implode('<br>', $links);
            }
            
            // Display programs.
            $programs = $entry->get_programs();
            if (!empty($programs)) {
                $links = array();
                foreach ($programs as $program) {
                    $links[] = html_writer::tag('a', $program->fullname, array('href' => $program->url));
                }
                echo implode('<br>', $links);
            }
            
            echo html_writer::end_tag('div');
        }
        
        return ob_get_clean();
    }
    
    protected function display_calendar(&$results)
    {
        ob_start();
        
        $js_events = array();
        foreach ($results as $result) 
        {
            $entry = new \block_androgogic_catalogue\entry($result);
            if (!$entry->is_available_for_user()) {
                continue;
            }

            // Get course events.
            $courses = $entry->get_courses();
            if (!empty($courses)) {
                foreach ($courses as $course) {
                    $js_event = new stdClass();
                    $js_event->start = date('Y-m-d', $course->startdate);
                    $js_event->end = date('Y-m-d', $course->startdate);
                    $js_event->title = $course->shortname;
                    $js_event->url = (string) new moodle_url('/blocks/androgogic_catalogue/index.php', array('id' => $entry->id));
                    $js_event->allDay = TRUE;
                    $js_events[] = $js_event;
                }
            }
            
            // Get program events.
            $programs = $entry->get_programs();
            if (!empty($programs)) {
                foreach ($programs as $program) {
                    $js_event = new stdClass();
                    $js_event->start = date('Y-m-d', $program->availablefrom);
                    $js_event->end = date('Y-m-d', $program->availablefrom);
                    $js_event->title = $program->shortname;
                    $js_event->url = (string) new moodle_url('/blocks/androgogic_catalogue/index.php', array('id' => $entry->id));
                    $js_event->allDay = TRUE;
                    $js_events[] = $js_event;
                }
            }
        }
        
        echo '<div id="catalogue-calendar"></div>';
        echo "<script>
            jQuery(function($){
                $('#catalogue-calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },
                    firstDay: 1,
                    editable: true,
                    eventLimit: true, // allow 'more' link when too many events
                    events: ".json_encode($js_events)."
                });
            });
            </script>";

        return ob_get_clean();
    }
}
