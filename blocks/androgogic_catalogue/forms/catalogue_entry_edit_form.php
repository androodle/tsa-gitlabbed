<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

class catalogue_entry_edit_form extends moodleform {

    function definition() {
        
        global $DB;
        
        $mform =& $this->_form;
        
        $mform->addElement('html','<div>');

        $mform->addElement('hidden', 'tab', 'catalogue_entry_edit');
        $mform->setType('tab', PARAM_TEXT);
        
        $mform->addElement('hidden','id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', get_string('name','block_androgogic_catalogue'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', get_string('required'), 'required', null, 'server');

        $mform->addElement('date_selector', 'enddate', get_string('end_date','block_androgogic_catalogue'));

        $mform->addElement('editor', 'description_editor', get_string('description','block_androgogic_catalogue'), null, $this->_customdata['editoroptions']);
        $mform->setType('description_editor', PARAM_RAW);
        
        $mform->addElement('selectyesno', 'public', get_string('public','block_androgogic_catalogue'));

        $sql = "select id, fullname from mdl_course where visible = 1 and id != 1 ORDER BY fullname";
        $results = $DB->get_records_sql_menu($sql);
        $select = $mform->addElement('select', 'course_id', get_string('course', 'block_androgogic_catalogue'), $results,array('class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('prog',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'program_id', get_string('program','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);
        
        $mform->addElement('header', 'visibilityfieldset', get_string('visibilitysettings','block_androgogic_catalogue'));
        $mform->setExpanded('visibilityfieldset', false);

        $options = $DB->get_records_menu('org',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'organisation_id', get_string('organisation','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('pos',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'position_id', get_string('position','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('cohort',array(),'name','id,name');
        $select = $mform->addElement('select', 'cohort_id', get_string('cohort','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);
        
        $mform->addElement('header', 'filterfieldset', get_string('filtersettings','block_androgogic_catalogue'));
        $mform->setExpanded('filterfieldset', false);

        $options = $DB->get_records_menu('androgogic_catalogue_locations',array(),'name','id,name');
        $select = $mform->addElement('select', 'location_id', get_string('location','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('comp',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'competency_id', get_string('competency','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);
        
        $this->add_action_buttons(true);
        
        $mform->addElement('html','</div>');
    }

    function add_action_buttons ($cancel = true, $submitlabel=null) {
        global $id;
        if (is_null($submitlabel)){
            $submitlabel = get_string('savechanges');
        }
        $mform =& $this->_form;
        if ($cancel) {
            //when two elements we need a group
            $buttonarray=array();
            $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
            $buttonarray[] = &$mform->createElement('cancel');
            if (!isset($id)) {
                $buttonarray[] = &$mform->createElement('reset', 'resetbutton', get_string('reset'),  ' onclick="javascript:return multireset();"');
            }
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
            $mform->closeHeaderBefore('buttonar');
        } else {
            //no group needed
            $mform->addElement('submit', 'submitbutton', $submitlabel);
            $mform->closeHeaderBefore('submitbutton');
        }
    }

    function get_editor_options() {
        return $this->_customdata['editoroptions'];
    }

}
