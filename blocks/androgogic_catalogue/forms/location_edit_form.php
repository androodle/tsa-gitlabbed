<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

class location_edit_form extends moodleform {

    protected $location;

    function definition() {
        
        $mform =& $this->_form;
        
        $mform->addElement('html','<div>');

        $mform->addElement('hidden','tab');
        $mform->setType('tab', PARAM_TEXT);

        $mform->addElement('hidden','id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', get_string('name','block_androgogic_catalogue'), array('size' => 120));
        $mform->setType('name', PARAM_TEXT);

        $this->add_action_buttons(true);
        
        $mform->addElement('html','</div>');
    }
}
