<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

class catalogue_entry_search_form extends moodleform {
    
    function definition() {
        
        global $DB, $TOTARA_COURSE_TYPES;

        $mform =& $this->_form;
        
        $mform->addElement('html','<div>');
        //search controls
        $mform->addElement('text','q',get_string('catalogue_entry_search_instructions', 'block_androgogic_catalogue'));
        $mform->setType('q', PARAM_TEXT);

        $dboptions = $DB->get_records_menu('androgogic_catalogue_locations',array(),'name','id,name');
        $options = array();
        $options[0] = 'Any';
        foreach ($dboptions as $key=>$value) {
            $options[$key] = $value;
        }
        $select = $mform->addElement('select', 'l', get_string('location','block_androgogic_catalogue'), $options);

        $dboptions = $DB->get_records_menu('comp',array(),'fullname','id,fullname');
        $options = array();
        $options[0] = 'Any';
        foreach ($dboptions as $key=>$value) {
            $options[$key] = $value;
        }
        $select = $mform->addElement('select', 'c', get_string('competency','block_androgogic_catalogue'), $options);

        //Course type
        $coursetypeoptions = array();
        $coursetypeoptions[-1] = 'Select';
        foreach ($TOTARA_COURSE_TYPES as $k => $v) {
            $coursetypeoptions[$v] = get_string($k, 'totara_core');
        }
        $mform->addElement('select', 'ct', get_string('coursetype', 'totara_core'), $coursetypeoptions);

        // date filters
        $mform->addElement('date_selector', 'ds', get_string('startdate', 'block_androgogic_catalogue'), array('optional'=>true));
        $mform->addElement('date_selector', 'de', get_string('enddate', 'block_androgogic_catalogue'), array('optional'=>true));

        //hiddens
        $mform->addElement('hidden','tab');
        $mform->setType('tab', PARAM_TEXT);

        $mform->addElement('hidden','sort');
        $mform->setType('sort', PARAM_TEXT);

        $mform->addElement('hidden','dir');
        $mform->setType('dir', PARAM_TEXT);
        
        $this->add_action_buttons();

        $mform->addElement('html','</div>');
    }

    function add_action_buttons ($cancel = true, $submitlabel=null) {
        
        $mform =& $this->_form;
        
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('search'));
        $buttonarray[] = &$mform->createElement('button', 'cancelbutton', get_string('cancel'),  ' onclick="javascript:location.href=\'index.php\'";');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
    
}
