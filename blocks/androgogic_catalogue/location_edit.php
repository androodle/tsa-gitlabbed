<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

require_once('../../config.php');
require_once('lib.php');
require_once('forms/location_edit_form.php');

$id = optional_param('id', null, PARAM_INT);

$context = context_system::instance();
$pageurl = new moodle_url('/blocks/androgogic_catalogue/location_edit.php');

$PAGE->set_context($context);
$PAGE->set_url($pageurl, compact('edit'));
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('location_search', 'block_androgogic_catalogue'));
$PAGE->requires->css('/blocks/androgogic_catalogue/css/multi-select.css');
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$PAGE->navbar->add(get_string('catalogue_entry_search','block_androgogic_catalogue'), "$CFG->wwwroot/blocks/androgogic_catalogue/index.php");

$returnurl = new moodle_url('/blocks/androgogic_catalogue/locations.php');

require_capability('block/androgogic_catalogue:edit', $context);

$location = new \block_androgogic_catalogue\location();
if (!empty($id)) {
    try {
        $location->load($id);
    } catch (Exception $ex) {
        throw $ex;
    }
}

$mform = new location_edit_form();
$mform->set_data($location);

if ($mform->is_cancelled()) {
    redirect($returnurl);
}

$data = $mform->get_data();
if ($data) {
    $location->save($data);
    redirect($returnurl, get_string('datasubmitted', 'block_androgogic_catalogue'));
}

include_once('tabs.php');

echo $OUTPUT->header();
echo $OUTPUT->tabtree($tabs, 'locations');
echo $OUTPUT->heading(get_string('location_edit', 'block_androgogic_catalogue'));

$mform->display();

echo $OUTPUT->footer();
