<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

function block_androgogic_catalogue_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload) {
    
    global $DB;
    
    $sql = "select * 
        from {files} 
        where itemid = ? 
            and filename = ?
            and contextid = ? 
            and filearea = ?";
    $params = array(
        $args[0],
        $args[1],
        $context->id,
        $filearea,
    );
    
    $file = $DB->get_record_sql($sql, $params);
    
    if (!$file) {
        send_file_not_found();
    } else {
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }

    $forcedownload = true;
    session_get_instance()->write_close();
    send_stored_file($stored_file, 60 * 60, 0, $forcedownload);
}
