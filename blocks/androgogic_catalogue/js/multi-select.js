
/**
 * See Doco at http://loudev.com/
 */

$('#id_organisation_id').multiSelect({
    selectableHeader: "<div class='ms-ag-selectable'>&nbsp;Available: <input type='text' class='search-input' autocomplete='off' placeholder='search...'></div>",
    selectionHeader: "<div class='ms-ag-selected'>&nbsp;Selected: <input type='text' class='search-input' autocomplete='off' placeholder='search...'></div>",
    afterInit: function(ms){
      var that = this,
          $selectableSearch = that.$selectableUl.prev().children().first(),
          $selectionSearch = that.$selectionUl.prev().children().first(),
          selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
          selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40){
          that.$selectableUl.focus();
          return false;
        }
      });

      that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
      .on('keydown', function(e){
        if (e.which == 40){
          that.$selectionUl.focus();
          return false;
        }
      });
    },
    afterSelect: function(value){
      seldesel(value, 'select');
      this.qs1.cache();
      this.qs2.cache();
    },
    afterDeselect: function(value){
      seldesel(value, 'deselect');
      this.qs1.cache();
      this.qs2.cache();
    }
});


$('.multiselect').multiSelect({
    selectableHeader: "<div class='ms-ag-selectable'>&nbsp;Available: <input type='text' class='search-input' autocomplete='off' placeholder='search...'></div>",
    selectionHeader: "<div class='ms-ag-selected'>&nbsp;Selected: <input type='text' class='search-input' autocomplete='off' placeholder='search...'></div>",
    afterInit: function(ms){
      var that = this,
          $selectableSearch = that.$selectableUl.prev().children().first(),
          $selectionSearch = that.$selectionUl.prev().children().first(),
          selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
          selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40){
          that.$selectableUl.focus();
          return false;
        }
      });

      that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
      .on('keydown', function(e){
        if (e.which == 40){
          that.$selectionUl.focus();
          return false;
        }
      });
    },
    afterSelect: function(){
      this.qs1.cache();
      this.qs2.cache();
    },
    afterDeselect: function(){
      this.qs1.cache();
      this.qs2.cache();
    }
});

function seldesel(orgid, action) {
    $.ajax({
        url: "/blocks/androgogic_catalogue/ajax_getchildorgs.php",
        type: 'post',
        dataType: 'json',
        data: "id=" + orgid,
        async: true,
        success: function(returnval) {
            $('#id_organisation_id').multiSelect(action, returnval);
        },
    });
}

function multireset() {
    $('.multiselect').multiSelect('deselect_all');
    return true;
}