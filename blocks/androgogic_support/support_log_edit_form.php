<?php

/** 
 * Androgogic Support Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     07/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class support_log_edit_form extends moodleform {
protected $support_log;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$page_before_support = optional_param('page_before_support', '', PARAM_RAW);
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user 
from mdl_androgogic_support_log a 
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
where a.id = {$_REQUEST['id']} ";
$support_log = $DB->get_record_sql($q);
}
else{
$support_log = $this->_customdata['$support_log']; // this contains the data of this form
}
$tab = 'support_log_new'; // from whence we were called

$mform->addElement('html','<div>');

//first_name
$mform->addElement('text', 'first_name', get_string('first_name','block_androgogic_support'), array('size'=>50));
$mform->addRule('first_name', get_string('required'), 'required', null, 'server');
$mform->addRule('first_name', 'Maximum 50 characters', 'maxlength', 50, 'client');

//last_name
$mform->addElement('text', 'last_name', get_string('last_name','block_androgogic_support'), array('size'=>50));
$mform->addRule('last_name', get_string('required'), 'required', null, 'server');
$mform->addRule('last_name', 'Maximum 50 characters', 'maxlength', 50, 'client');

//email
$mform->addElement('text', 'email', get_string('email','block_androgogic_support'), array('size'=>50));
$mform->addRule('email', get_string('required'), 'required', null, 'server');
$mform->addRule('email', 'Maximum 50 characters', 'maxlength', 50, 'client');
$mform->addRule('email', 'Please enter a valid email address', 'email', null, 'server');

//contact_number
$mform->addElement('text', 'contact_number', get_string('contact_number','block_androgogic_support'), array('size'=>20));
$mform->addRule('contact_number', get_string('required'), 'required', null, 'server');
$mform->addRule('contact_number', 'Maximum 20 characters', 'maxlength', 20, 'client');

//problem_description
$mform->addElement('text', 'problem_description', get_string('problem_description','block_androgogic_support'), array('size'=>50));
$mform->addRule('problem_description', get_string('required'), 'required', null, 'server');
$mform->addRule('problem_description', 'Maximum 50 characters', 'maxlength', 50, 'client');

//steps_to_reproduce
$editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
$mform->addElement('editor', 'steps_to_reproduce', get_string('steps_to_reproduce','block_androgogic_support'), null, $editoroptions);
$mform->setType('steps_to_reproduce', PARAM_CLEANHTML);
$mform->addRule('steps_to_reproduce', get_string('required'), 'required', null, 'server');

//uploaded_file_id
$mform->addElement('filepicker', 'uploaded_file_id', get_string('uploaded_file','block_androgogic_support'));


//set values if user is logged in, and if we are coming in fresh
if (isset($USER->id) && !isset($_POST['sesskey'])) {
    $mform->setConstant('first_name', $USER->firstname);
    $mform->setConstant('last_name', $USER->lastname);
    $mform->setConstant('email', $USER->email);
    $mform->setConstant('contact_number', $USER->phone1);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','page_before_support',$page_before_support);

$this->add_action_buttons();
$mform->addElement('html','</div>');
}

    // A slightly modified parent function to let us
    // customize the Cancel button behaviour.
    function add_action_buttons($cancel = true, $submitlabel=null){
        if (is_null($submitlabel)){
            $submitlabel = get_string('savechanges');
        }
        $mform =& $this->_form;
        if ($cancel){
            //when two elements we need a group
            $buttonarray=array();
            $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
            $cancel_url = new moodle_url('/blocks/androgogic_support', array('tab' => 'server_status'));
            $buttonarray[] = &$mform->createElement('button', 'cancel', get_string('cancel'), ' onclick="javascript:location.href=\''.$cancel_url.'\';"');
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
            $mform->closeHeaderBefore('buttonar');
        } else {
            //no group needed
            $mform->addElement('submit', 'submitbutton', $submitlabel);
            $mform->closeHeaderBefore('submitbutton');
        }
    }
}
