<?php

/** 
 * Androgogic Support Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     07/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class support_log_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('support_log_search_instructions', 'block_androgogic_support'));
$dboptions = $DB->get_records_menu('user',array(),'lastname',"id,concat(firstname, ' ', lastname) as fullname");
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'user_id', get_string('user','block_androgogic_support'), $options);
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
