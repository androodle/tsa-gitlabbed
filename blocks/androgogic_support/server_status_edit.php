<?php

/** 
 * Androgogic Support Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the server_statuses
 *
 **/

global $OUTPUT;

require_capability('block/androgogic_support:edit', $context);

require_once('server_status_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_androgogic_server_status a 
where a.id = $id ";
$server_status = $DB->get_record_sql($q);
$mform = new server_status_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->summary = format_text($data->summary['text'], $data->summary['format']);
$DB->update_record('androgogic_server_status',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_support'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('server_status_edit', 'block_androgogic_support'));
$mform->display();
}

?>
