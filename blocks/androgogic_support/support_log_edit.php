<?php

/** 
 * Androgogic Support Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     07/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the support_logs
 *
 **/

global $OUTPUT;

require_capability('block/androgogic_support:edit', $context);

if (!is_siteadmin()) {
    error('Only for admins');
}

require_once('support_log_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user 
from mdl_androgogic_support_log a 
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
where a.id = $id ";
$support_log = $DB->get_record_sql($q);
$mform = new support_log_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->steps_to_reproduce = format_text($data->steps_to_reproduce['text'], $data->steps_to_reproduce['format']);
$DB->update_record('androgogic_support_log',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_support'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('support_log_edit', 'block_androgogic_support'));
$mform->display();
}

?>
