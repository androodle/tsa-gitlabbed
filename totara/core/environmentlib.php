<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Petr Skoda <petr.skoda@totaralms.com>
 * @package totara_code
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Check that the Totara build date always goes up.
 * @param environment_results $result
 * @return environment_results
 */
function totara_core_linear_upgrade_check(environment_results $result) {
    global $CFG;
    if (empty($CFG->totara_build)) {
        // This is a new install or upgrade from Moodle.
        return null;
    }

    $result->info = 'linear_upgrade';

    $TOTARA = new stdClass();
    $TOTARA->build = 0;
    require("$CFG->dirroot/version.php");

    if ($TOTARA->build < $CFG->totara_build) {
        $result->setRestrictStr(array('upgradenonlinear', 'totara_core', $CFG->totara_build));
        $result->setStatus(false);
        return $result;
    }

    // Everything is fine, no need for any info.
    return null;
}
