<?php
/*

Totara LMS Changelog

Release 2.6.19 (21st April 2015):
==================================================

Security issues:
    T-13359        Improved param type handling in Reportbuilder ajax scripts

New features:
    T-13624        Added new OpenSesame integration plugin

                   OpenSesame is a publisher of online training courses for business. This new
                   plugin allows you to sign up for OpenSesame Plus subscription and access
                   the large catalogue directly from your Totara site. To start the
                   registration go to "Site administration / Courses / OpenSesame / Sign up
                   for access". After the content packages are downloaded to your site you can
                   either select them when adding SCORM activities or use the Create courses
                   link.

                   Additional information will be made available in the coming days, if you have any
                   queries please contact your partner channel manager.

Improvements:
    T-13925        Improved sql case sensitive string comparisons for completion import records.

                   When 'Force' is enabled the system keeps track of the course shortnames
                   being used, each new course is compared in a case sensitive way and if it
                   matches that but doesn't match exactly the system uses the first course
                   name instance. There will be no error messages on the import report about
                   it.

    T-14217        Added debug param to all Reportbuilder reports

                   This allows administrators to retrieve additional debugging information if
                   they have problems with reports. Some reports already had this feature, now
                   all of them do.

    T-13859        Added functionality to allow administrators to manage Facetoface session reservations

                   If a manager reserved spaces for a their team in a Facetoface session and
                   then they either left or changed role, there was no way to edit/remove the
                   reservations they made.

    T-14263        Improved the display of User full names for Programs
    T-14155        Changed "No time limit" to "No minimum time" in Program course sets

                   There was confusion about the purpose of this setting. Some people thought
                   that "No time limit" meant that it would allow users to spend as long as
                   they wanted to complete a program, but user time allowance is controlled in
                   the assignments tab. Setting "No minimum time" will prevent time allowance
                   exceptions (except where a relative assignment completion date would be in
                   the past, such as "5 days after first login" being set on a user who first
                   logged in one month ago).

    T-13491        Fixed immediate updating of Audience enrolments after changes in dynamic audience rules
    T-14016        Added "Choose" option as the default value for "Menu of choices" type custom fields
    T-14254        Added description field to the badge criteria
    T-14201        Improved display of long text strings in the Alerts block
    T-14212        Restored instant completion for course completion criteria

                   Instant course completion was introduced in Totara 2.5 but was mistakenly
                   removed in 2.6.0. This fix restores instant course completion (when the
                   criteria completion depends on other courses) which also improves the
                   performance of the completion cron tasks.

    T-13542        Changed language pack, timezone and help site links to use HTTPS

API Changes:
    T-14242        Added finer grained control to Scheduler form element

                   Every X hours and Every X minutes were added to the scheduler form element
                   which is used for scheduled reports, Report Builder cache refresh and Totara Sync.
                   The frequency that these events occurs is still limited by the
                   frequency at which cron is run. E.g. if your cron is set to run every 10
                   minutes then any event scheduled for Every X minutes, where X is less than
                   10, will actually only occur once every 10 minutes. Any customisation using
                   the scheduler form element will also be affected by this enhancement.

    T-14215        Enforced linear date paths when upgrading Totara

                   When upgrading a Totara installation you are now required to follow a
                   linear release date path. This means that you now MUST upgrade to a version
                   of the software released at the same time or after the release date of your
                   current version.

                   For example if you are currently running 2.5.25 which was released on 18th
                   March 2015, you can upgrade to any subsequent version of 2.5, any 2.6 from
                   2.6.18 onwards, or any 2.7 from 2.7.1 onwards.

                   This ensures that sites never end up in a situation where they are missing
                   critical security fixes, and are never in a state where the database
                   structure may not match the code base.

                   The Release Notes forum is a useful source for the date order of version
                   releases https://community.totaralms.com/mod/forum/view.php?id=1834

Bug Fixes:
    T-14311        Fixed the archiving of Facetoface session signups and completion

                   When uploading historic certification completions future facetoface session
                   signups were being archived, now it will only archive signups that
                   completed before the window opens date.

    T-14065        Removed non-functional "deleted user" filter from bulk user action pages
    T-14029        Fixed cohort types when upgrading to Totara from Moodle

                   After upgrading from Moodle to Totara it was not possible to edit members
                   from previously created Moodle Cohorts, that now appeared in Totara
                   Audiences.

    T-13809        Fixed dialog content when trying to select a visible audience for a Program

                   Homepage incorrectly opens in dialog box when trying to select a visible
                   audience for a program

    T-12583        Fixed behaviour of expand/collapse link in the standard course catalog
    T-13550        Fixed Due Date displaying as 1970 on Record of Learning: Certifications

                   This patch fixed a problem where an empty timedue data value was causing a
                   date in 1970 to show in the Due Date field in the Record of Learning:
                   Certifications report source. It also introduces a comprehensive phpunit
                   test for the certification and recertification process.

    T-13853        Fixed handling of position, organisation, and manager when a validation error occurs in email based self registration
    T-13879        Fixed grade filter when checking against RPL grades
    T-14184        Fixed appending of message body after manager prefix in Facetoface 3rd party notifications

                   If Third party email addresses were specified in the Facetoface settings then any
                   third-party copies of emails sent by the system would not contain both of the
                   manager prefix and the body of the message (the content that the learner receives)

    T-14235        Fixed Audience rulesets for Course completion and Program completion
    T-14105        Fixed calculation of 'User Courses Started Count' in the 'User' report source

                   This is now calculated based on course completion records, rather than
                   block_totara_stats (which might be missing 'started' records for various
                   reasons, such as migration from Moodle or changing certain completion
                   settings while a course is active). This affects the 'Courses Started'
                   column in the 'My Team' page - courses started should now always be greater
                   than or equal to courses completed.

    T-14187        Fixed resetting of Hierarchy item types after every Totara Sync run

                   Hierarchy item (e.g. position, organisation etc) types should only be changed
                   if the Sync source contains the "type" column.

    T-14284        Changed Record of Learning: Courses report source to use enrolment records

                   Previously, it used role assignment records, but roles can be granted
                   without the user being enrolled, and only indicated some level of
                   capability within the course rather than participation. The whole query was
                   also rewritten to improve performance.

                   Thanks to Eugene Venter from Catalyst for this contribution

    T-14338        Fixed user fullname handling in Facetoface when editing attendees
    T-14243        Fixed handling of special characters (e.g. &) when returning selections from dialogs
    T-14366        Fixed Reportbuilder expanded sections when multiple params are used

                   Thanks to Andrew Hancox from Synergy Learning for this contribution

    T-14192        Fixed Assignment Submissions report when it is set to use 'Scale' grade

                   The Submission Grade, Max Grade, Min Grade and Grade Scale values columns
                   were displaying incorrect information.

    T-14176        Fixed "Attempts re-opened - Automatically until pass" setting for Assignments

                   In the "Submission settings" if "Attempts reopened" was set to
                   "Automatically until pass" this setting had no effect and learners were not
                   given new attempts after an initial failed attempt

    T-13890        Fixed date change emails being incorrectly sent when a Facetoface session capacity is changed
    T-13917        Fixed error when adding Score column to Appraisal Details report

                   The report would fail if the Score column was added to an Appraisal
                   containing a Rating (Numeric scale) question.

    T-13886        Fixed PHP Notice messages when adding a Rating (custom scale) question to Appraisals
    T-14266        Fixed the display of Learning Plans date started when viewing a completed plan
    T-13742        Fixed uploading of Organisation and Position custom fields via Totara Sync
    T-13353        Fixed handling of param options in Reportbuilder
    T-13894        Fixed validation and handling of Facetoface notification titles
    T-13993        Fixed error when uploading users with the deleted flag set

                   When uploading users via the upload users function, if a user in the CSV
                   file had the deleted flag set, and was already marked as deleted in the
                   system, an invalid user error was generated and this halted all processing
                   of the CSV file.

    T-14048        Fixed incorrect Quiz question bank names after previous upgrade

                   Question banks containing subcategories had the subcategory names
                   incorrectly changed by a change in 2.5.20 and 2.6.12. If you are upgrading
                   from a version affected (2.5.20-25, 2.6.12-18) you may want to check if
                   your Quiz question bank categories are affected.

    T-14186        Fixed max length validation for Question input text box
    T-13009        Fixed incorrect creation of multiple custom room records when saving a Facetoface session
    T-14131        Fixed handling of error when importing users via Totara Sync with the create action disabled

                   Thanks to Andrew Hancox at Synergy Leaning for contributing the solution.

    T-14302        Fixed database host validation in Totara Sync settings
    T-13846        Fixed the default role setting for the Program Enrolment plugin
    T-14083        Fixed display of 'More info' Facetoface session page

                   When a session was fully booked, learners booked on the session would
                   receive a ""This session is now full" message instead of the actual details
                   of the session.

    T-13845        Fixed setting position fields on email-based self authentication
    T-14075        Fixed inability to send a previously declined Learning Plan for re-approval


Release 2.6.18 (18th March 2015):
==================================================

Security issues:
    MoodleHQ       Security fixes from MoodleHQ http://docs.moodle.org/dev/Moodle_2.6.10_release_notes
    T-13996        Removed potential CSRF when setting course completion via RPL
    T-14175        Fixed file access in attachments for Record Of Learning Evidence items
                   Thanks to Emmanuel Law from Aura Infosec for reporting this issue

Improvements:
    T-13824        Added course request buttons to the enhanced catalog

                   When "Enable course requests" is turned on, the "Request a course" and
                   "Courses pending approval" buttons will be displayed in the enhanced
                   catalog, as they are in the old course catalog.

    T-13951        Implemented sql_round dml function to fix MySQL rounding problems
    T-13867        Added "is not empty" option to text filters in Reportbuilder
    T-13370        Fusion report builder export is no longer enabled by default

API Changes:
    T-14084        Renamed $onlyrequiredlearning parameter in prog_get_all_programs and prog_get_required_programs functions
    T-13966        Improved performance for assigning large groups to Programs

                   Moved the Program assignment messages from the program_assigned event
                   observer to the cron, and removed the then empty observer.

    T-14104        Added courses which have completion records to the record of learning

                   Previously, if a user had been enrolled into a course, made some progress
                   or completed it, then been unenrolled from the course, the record of course
                   participation disappeared from the user's record of learning. With this
                   patch, courses will still show when a user has been unenrolled, if their
                   course status was In Progress, Complete or Complete by RPL. This change was
                   made to the Record of Learning: Courses report source, so all reports based
                   on this source will be affected.

Bug Fixes:
    T-13960        Fixed Totara Sync when using files that contain a UTF byte order marker

                   Additionally, this patch also improves error checking in relation to
                   setting configuration.

    T-14059        Fixed timemodified / timecreated database anomalies

                   In certain circumstances Facetoface session and hierarchy item records
                   could end up with the timemodified timestamp being before timecreated.

    T-12991        Fixed Facetoface manager reservations with multiple bookings

                   Before, if a learner had been assigned a reserved place they could not be
                   assigned to any other sessions; even if multiple sign-ups was on.

                   Now, it should allow managers to assign members of their staff to multiple
                   sessions if "Allow multiple sessions signup per user" setting is on, but
                   not allow more than one user assignment per Facetoface activity if that
                   setting is off.

    T-13920        Fixed export to Excel of completion progress column in Record Of Learning - Programs report source
    T-14001        Fixed "Same as preceding question" checkbox in IE8 when assigning roles to a question in Appraisals
    T-13698        Fixed display of required learning review questions when learners are completing an Appraisal
    T-13995        Removed unused files from codebase

                   The following files have been removed
                   * course/completion_dependency.php
                   * totara/core/js/completion.dependencies.js.php

    T-11338        Fixed saving of options when using multiple choice questions in Appraisals
    T-13986        Fixed deletion of course custom field data when a course is deleted
    T-13366        Fixed certifications incorrectly causing the programs tab to appear in Record of Learning
    T-13884        Fixed error in Appraisals dialog box when selecting required learning for review
    T-14114        Fixed program and certification exceptions being regenerated after being resolved

                   This patch also prevents certification exceptions being generated when an
                   assignment date is in the past and the user is in the recertification stage
                   (at which point the assignment date is not relevant, as the due date is
                   controlled by the certification expiry date instead).

    T-14093        Fixed dynamic audiences rules based on Position and Organisation custom fields

                   Thanks to Eugene Venter at Catalyst for contributing to this

    T-13628        Fixed deletion of custom fields if missing from the Totara Sync CSV file
    T-13926        Fixed copying instance data when copying block instances

                   When users click the "Customise this page" button in My Learning, blocks
                   copied from the default My Learning page to the user's personal My Learning
                   page can also copy instance specific data. This allows Quick Links blocks
                   to correctly copy the default URLs.

    T-14070        Fixed MySQL 5.6 compatibility issues

                   Totara would not install on MySQL 5.6, and also unit tests were failing
                   with "Specified key was too long" errors

    T-13946        Fixed change password function for users with an apostrophe in their username
    T-14120        Fixed sort in all Audience dialog boxes
    T-14079        Fixed visibility of course items in Learning Plan page

                   There were several problems relating to being able to add courses to a
                   learning plan that are not visible to the learner. This fix is preventing
                   an admin to add invisible courses and taking into account audience
                   visibility.

    T-14142        Fixed display of Submission Feedback Comment and Last modified date in Assignment Submissions report source
    T-13472        Fixed several problems where scheduled Appraisal messages were not sent at the right times
    T-14135        Fixed paging when adding items to a learning plan
    T-13983        Ensured Totara Sync continues processing users if it discovers a problem

                   There were several situations where a user record could pass the sanity
                   checks, but could cause an error when being added to the database. These
                   errors will no longer halt the processing of the user sync. Also, warning
                   and error output has been improved.

    T-14041        Fixed installation on PostgreSQL when the dbschema option is present

                   Added support for custom schemas in PostgreSQL

    T-14158        Fixed the display of Facetoface sessions spanning several days on the calendar

                   Thanks to Eugene Venter at Catalyst for contributing to this

    T-14098        Fixed visibility of hidden/disabled Certifications and Programs in Audience Enrolled Learning
    T-13984        Fixed editing/deleting of blocks when viewing a Choice activity module

                   Thanks to Ben Lobo at Kineo for contributing to this

    T-14125        Fixed compatibility of iCal email attachments with some SMTP servers
    T-14074        Fixed theme precedence issues

                   Currently if you set a mobile theme in Site Administration > Appearance >
                   Themes > Theme Selector, the mobile theme will take precedence over any
                   user, course or category themes when viewing Totara on a mobile device.
                   This patch reverses this (so that User, Course and Category themes will
                   take precedence over a mobile theme).

                   If you wish to maintain the current (pre patch) behaviour add the line
                   "$CFG->themeorder = array('device', 'course', 'category', 'session',
                   'user', 'site');" to your config.php file

    T-13661        Fixed joinlist for the Assignment Submissions report source

                   When viewing the 'Assignment submissions' report source, no assignment
                   submissions were displayed unless they were either graded, or the
                   'Submission grade' column was removed from the report.

    T-14072        Fixed intermittent query parameters error when using the select course dialog
    T-14000        Fixed changing of custom field order in Program custom fields
    T-13630        Fixed MSSQL ORDER BY for audience rule set when using a custom text input field with 'Choose' option
    T-13922        Fixed Totara Sync error messages to display strings over 255 chars long
    T-14088        Fixed incorrect reference to course completion data in delete user confirmation text
    T-14027        Prevented users from deleting the default category
    T-13895        Fixed bug where temporary managers were being incorrectly removed by cron


Release 2.6.17 (18th February 2015):
==================================================

Improvements:
    T-13071        Created separate report sources for certification completion/overview

                   The logic in program reports was not up to the task of displaying
                   certifications, so they have been moved into their own report sources. The
                   "Program Overview" report source is now mirrored by "Certification
                   Overview", and "Program Completion" by "Certification Completion". If you
                   are currently using program reports to display certifications then you will
                   need to create the certification versions of the reports, where you will
                   find all the same functionality plus some new certification specific fields
                   and functionality.

    T-13854        Added additional information to 'Delete user' confirmation page

API Changes:
    T-14043        Added ability to unlock activity completion without deletion

                   This change also fixes a bug whereby a user editing an activity included in
                   course completion but not yet completed by any users causes all users who
                   have previously completed the course to loose their data.

Bug Fixes:
    T-13533        Fixed inaccessible embedded reports being displayed in My Reports

                   Reports unavailable due to capability checks were not being filtered out.

    T-13888        Fixed displayed value of Course Date Completed column in Program Overview report source
    T-13603        Fixed sending of Facetoface session trainer cancellation notifications to all assigned trainers
    T-14013        Fixed validation bug when copying Facetoface session with predefined room
    T-13952        Fixed display of Totara Menu with custom CSS applied in Custom Totara and Custom Totara Responsive themes
    T-13885        Fixed display of HTML encoded characters when added via a dialogue box

                   When editing a users position (primary or otherwise), if an organisation,
                   position or user is added to the users position via a dialogue box and the
                   data added contains HTML encoded characters (such as an ampersand - &amp;)
                   the character was displayed in its encoded form (i.e. &amp;)
                   rather than its non-encoded form (i.e. &). This has been corrected.

    T-13988        Fixed issue with timezone calculations incorrectly detecting session role conflicts in Facetoface
    T-13812        Fixed several interface problems related to editing rules in dynamic audiences
    T-13744        Fixed unread count for tasks and alerts in Messaging block
    T-14042        Fixed cleanup of users enrolled on a certification by an audience, who are no longer part of the audience
    T-13910        Fixed user checkbox filter when searching list of users
    T-13887        Fixed saved searches being incorrectly applied on scheduled reports
    T-13619        Fixed RTL display issues in admin tables in Standard Totara Responsive theme
    T-13433        Fixed calculation across DST boundaries for Reportbuilder and Sync next scheduled run

                   Scheduled daily Reportbuilder reports and scheduled daily Sync runs would
                   trigger repeatedly on every cron run for an hour during DST boundaries
                   where the clocks are going back/forward an hour.

    T-13582        Fixed 'Currently Viewing' navigation section when Learning Plans are disabled
    T-13724        Excluded inaccessible courses and programs from being added to learning plans
    T-13881        Fixed iCal description fomatting issue where spaces between words could be removed
    T-13190        Fixed users being incorrectly enrolled onto unavailable programs
    T-13599        Fixed Facetoface Waitlisted tab incorrectly being disabled after adding/removing attendees
    T-13936        Fixed appearance of buttons in quiz submission confirmation dialog

                   When custom button colour CSS is applied to the Custom Totara Responsive
                   theme, some buttons on dialogs would appear incorrectly.

    T-13987        Fixed error message when deleting a user custom profile field


Release 2.6.16.1 (2nd February 2015):
==================================================

Security issues:
    MDL-48980      Always clean the result from min_get_slash_argument
    T-13866        Ensured require_login is called before course visibility checks

Improvements:
    T-13873        Added notification that course completion criteria changes will be applied on next cron

Bug Fixes:
    T-13755        Fixed link for changing messages preferences in emails sent by the system
    T-13871        Fixed display of tabs in course grades Course Outcomes page
    T-12741        Converted the SQL params within badges_get_user_badges to named params.

                   Only affects badges - the badges_get_user_badges function is using
                   concatenation to form an SQL statement, an action which could potentially
                   be exploited if this function is called incorrectly. The fix is to use
                   named params and is entirely constrained within the function itself. It
                   should have no impact on functionality or customised uses of this function.

    T-13899        Fixed weekly scheduling of Reportbuilder reports when using non-English language

                   If scheduled reports have been sending on every cron run on your site then,
                   once this patch is installed, they will send one more time on the next cron
                   run before being correctly rescheduled.

    T-13868        Changed display of times to 24-hour format in non-English languages

                   The %p date format modifier to display AM/PM with time data in the 12-hour
                   clock format is unreliable across platforms and locales so we have switched
                   to 24-hour time display for most non-English languages. You can change this
                   back by making a local language customisation to the
                   nice_time_in_timezone_format and timedisplay24 strings in
                   totara_reportbuilder, and the sessiondatetimeformat string in
                   mod_facetoface, using the formats described here
                   http://php.net/manual/en/function.strftime.php

    T-13006        Fixed users in limbo due to facetoface session signup capacity collisions

                   It is possible, although highly unlikely, that two users tried to sign up
                   to the last place in a session at the same time, and one of the users
                   became stuck in limbo, neither properly assigned nor able to sign up to
                   another session. This patch fixes existing records and prevents the error
                   from occurring.


Release 2.6.16 (21st January 2015):
==================================================

Security issues:
    MoodleHQ       Security fixes from MoodleHQ http://docs.moodle.org/dev/Moodle_2.6.7_release_notes

Improvements:
    T-12100        Added the ability to assign a certification to an audience under enrolled learning
    T-13477        Improved scalability for the program cron and reports

                   Thanks to Kineo UK for providing the core of this patch

    T-11141        Added the ability to use spaces in field names in a CSV file for Totara Sync
    T-13653        Improved behaviour of Facetoface session duration in relation to session date/time

                   The session duration field is now disabled when session date/time is known,
                   and is automatically recalculated (as before) when the session is saved

API Changes:
    T-13636        Fixed "from" address in Face-to-face waitlist emails when a user cancels their booking

                   The optional param $fromuser has been added to several
                   facetoface_user_signup and several Face-to-face notification functions.

Bug Fixes:
    T-13647        Fixed Overall Total columns in Appraisal Detail report source
    T-13552        Fixed duplicate records in Program Completion reports

                   This patch removes duplicate records from tables prog_completion and
                   prog_user_assignment. Deleted program completion records are archived in
                   prog_completion_history. Indexes are added to these tables to prevent
                   future duplication of records and discrepancies in the record of learning
                   and required learning reports.

    T-13880        Fixed missing language string in Facetoface notifications
    T-12679        Fixed completion date on a course with multiple Facetoface sessions

                   If a course contained a Facetoface session with multiple sessions where a
                   user could complete the activity multiple times (for example a course used
                   as part of a recurring certification) the course completion date would
                   always use the date of the earliest session the user completed, not their
                   most recent completion

    T-13422        Fixed archiving of completion on certifications containing a Facetoface

                   In some circumstances if a certification path contained a course with a
                   Facetoface activity, course and activity completions would not be reset
                   properly when the recertification window opened, making it impossible to
                   recertify.

    T-13819        Changed course completion criteria unlocking - no records are changed until save changes is clicked

                   Existing course completion records were being removed immediately upon
                   clicking the "Unlock criteria and delete existing completion data" button.
                   This change causes the deletion of data to be delayed until the Save
                   changes button is clicked. If the users changes their mind, they can click
                   Cancel to abort the data reset.

    T-13835        Fixed SCORM retriggering course completion during certification archive

                   In some circumstances if a certification contained a course with a SCORM
                   activity, when the certification window opened course completion would not
                   be archived and reset properly.

    T-13725        Fixed incorrect check when unassigning users from a program/certification

                   When removing users from certifications that were uploaded via the upload
                   completion tool, the role_assignments table was being checked, when the
                   correct check should be on prog_user_assignment.

    T-13794        Fixed Face-to-face session dialog search for pre-defined rooms
    T-13612        Made program position completion criteria consistent with audience rules

                   The existing Position Start Date program completion criteria was being
                   calculated using the time that the position was saved to the database, not
                   the Start Date field. Existing Position Start Date completion criteria have
                   been renamed to Position Assigned Date to reflect the actual behaviour. New
                   Position Start Date completion criteria will be calculated from the Start
                   Date field (which must be set, otherwise a "Completion time unknown"
                   exception will occur).

    T-11643        Fixed display of error message if a program extension request fails
    T-13822        Fixed additional name fields error on Learning Plan tab of Audiences
    T-13756        Fixed email filters on User report source

                   Added a filter "User's Email (Ignoring user display setting)" and fixed
                   filtering on email addresses where the search term contained the @ symbol

    T-13877        Fixed highlighting of signed-up sessions in Facetoface
    T-13748        Fixed alert block visibility if configured to display when no alerts exist
    T-13723        Fixed deletion of program categories

                   When managing the program catalog, trying to delete a program category
                   would not actually delete the category, and would also not give any error
                   message.


Release 2.6.15 (18th December 2014):
==================================================

Security issues:
    T-13694        Fixed security issues with Reportbuilder expanding content and instant filters

New features:
    T-13221        Added visibility controls to Appraisal redisplay questions

                   Thanks to Luke at TDM for providing code to help develop this patch.


Improvements:
    T-12796        Added a help button explaining which statuses are included in 'All Booked' on Facetoface notifications page
    T-13426        Fixed approval workflow in Learning Plans

                   When a learner has permission to approve their own Learning Plan they now
                   get an Activate button instead of Approve and Decline buttons

    T-13547        Improved editing and display of Program Summary, Summary files and Endnote fields.

                   These fields are now shown in the program Overview tab. Clicking the Edit
                   Program Details button is now working correctly.

    T-13585        Improved alignment of activity completion checkboxes
    T-13504        Improved responsiveness of the Facetoface calendar
    T-13418        Fixed issues with bulk adding attendees on a Facetoface session where manager approval is required

                   This changes the default behaviour when bulk adding attendees to a
                   Facetoface session. Now if Manager approval is required an approval request
                   will be sent. There is also a new check-box at the bottom of the list to
                   ignore the manager approval setting.

    T-13561        Improved performance of the My Reports tab

Bug Fixes:
    T-13581        Removed commas on course page for Facetoface room when room fields are empty
    T-13490        Fixed potential database compatibility issue in Facetoface reservations SQL
    T-13560        Fixed setting of Site Manager and User capabilities on fresh installs

                   When Totara is freshly installed on a server it sets up the capabilities
                   for each system role. It was found that the following capabilities were not
                   being applied to the site manager and user roles correctly:

                     - totara/plan:manageevidencetypes (Site Manager)
                     - totara/plan:editsiteevidence (Site Manager)
                     - totara/plan:editownsiteevidence (Site Manager and User)

                   This is now corrected for a fresh installation. For existing installations,
                   administrators need to manually edit the roles and allow the capabilities
                   listed above.

    T-12529        Fixed behaviour of eye icon in manage courses page when audience visibility is on
    T-12676        Fixed Totara Sync database source connections with non-alphanumeric passwords
    T-13012        Fixed behaviour of course link in the Enhanced Catalog
    T-13708        Fixed the position fields saving on the email based self registrations settings page
    T-13439        Restricted user search in Facetoface bookings block to those with required capabilities

                   The search form will only be visible to users who have the
                   'block/facetoface:viewbookings' capability in the context of at least one
                   other user, and search results are filtered to those they can access.

    T-13556        Fixed redirect to Record of Learning after a manager manually completes a learner's course in a Program
    T-13597        Fixed database query related to some Program due date and time allowance calculations.

                   If a program had more than one course set with a "THEN" condition between
                   them AND a learner assignment had a fixed or empty completion date, then it
                   was sometimes possible for an invalid program exception to be generated for
                   the learner, or (only with non-empty, fixed completion date) the due date
                   may have been incorrectly calculated (possibly giving the learner an
                   extended due date).

    T-12749        Fixed undefined constant warning while managing courses
    T-13779        Fixed Program function prog_get_all_users_programs to return correct program ids
    T-13727        Fixed date format for Program Completion and Program Overview reports
    T-13611        Fixed Audience ruleset for course completion and program completion
    T-11876        Fixed alignment of ordering arrows in Appraisals when JavaScript is disabled
    T-13735        Fixed display of Badge creation date
    T-13702        Fixed alignment of ordering arrows in Feedback360 when JavaScript is disabled
    T-13704        Fixed reordering Appraisal questions with JavaScript disabled
    T-13761        Fixed error when editing a Facetoface session that uses predefined rooms
    T-13737        Added message body after manager prefix in Facetoface notifications

                   Contributed by Aaron Barnes from Catalyst

    T-13620        Fixed Wait-listed, Attendees and Spaces Available column totals in Facetoface Summary report
    T-13618        Removed Development Planner link from My Learning Nav block when Learning Plans are hidden or disabled
    T-13179        Fixed display of Appraisal notifications that contain line breaks
    T-13158        Fixed display of Badge descriptions that contain line breaks
    T-12967        Fixed hardcoded string in the time completed filter for the Feedback Summary report source
    T-13499        Fixed bug in Record of Learning embedded reports capability checks
    T-13483        Fixed Facetoface notifications not being sent to managers when notify manager checkbox is selected
    T-13558        Fixed error when Recent Learning Block is the first block on a page when using the Kiwifruit Responsive theme
    T-13652        Fixed bugs with Facetoface session room validation

                   Session validation is now performed every time a session is saved. If
                   validation fails, the problem will be displayed in the form, allowing the
                   user to correct the problem. With this change, the room will no longer be
                   automatically removed every time the session date or time is changed.

    T-13610        Fixed use of Moodle language string on Plan Evidence pages

                   Using totara_plan strings instead allows greater flexibility in language
                   customisation support

    T-13485        Fixed course grade not updating instantly on activity completion

                   In a course with one or more activities that provide grades, and course
                   completion criteria that require activity completion and a minimum course
                   grade, the course grade was not being recalculated instantly after each
                   activity was completed.

    T-13589        Fixed learner description for Course and Program tabs on Learning Plan
    T-13539        Fixed date type custom user profile fields being overwritten with today's date when updating users with Totara Sync
    T-13119        Fixed importing of MS Outlook iCal files into the Calendar
    T-13596        Fixed multilang filter support in Recent Learning block
    T-13557        Fixed issue when taking attendance in Facetoface where incorrect failure message is displayed even on success

                   The AJAX query that is run when taking attendance for a Facetoface session
                   attendee incorrectly returns an error even when on success.


Release 2.6.14 (14th November 2014):
==================================================

Security issues:
    MoodleHQ       Security fixes from MoodleHQ http://docs.moodle.org/dev/Moodle_2.6.6_release_notes
    T-13465        Fixed access control when viewing archived certificates
    T-13145        Fixed potential security vulnerabilities when editing saved searches in Reportbuilder
    T-13146        Prevent guests from using the saved search feature in Reportbuilder

                   Totara's data manipulation policy is that guest users cannot make any
                   changes that will alter data

Bug Fixes:
    T-13529        Fixed fatal error when trying to view Course Completion Report
    T-13219        Fixed undefined index error when caching user report
    T-12300        Fixed incorrect visibility of Facetoface sessions in the calendar

                   Made visibility of Facetoface sessions in the calendar match the visibility
                   of the course containing the Facetoface session, when audience visibility
                   is enabled.

    T-13511        Fixed upgrade errors when the activitynames filter is enabled


Release 2.6.13 (4th November 2014):
==================================================

Security issues:
    T-12620        Fixed Facetoface access control issues

Improvements:
    T-12257        Standardised the behaviour of programs, certifications and courses catalogues
    T-13405        Added ORDER BY clause in report builder sql debug output

Bug Fixes:
    T-13371        Fixed incorrect calculation of manual course enrolment durations

                   SCOPE: when users were enrolled onto a course manually and an enrollment
                   duration was set, the calculation of the end date was incorrect if the
                   enrollment period crossed a daylight-savings boundary.

                   IMPACT: manual enrollment periods may have had an end date that was
                   incorrect by one hour

    T-13375        Fixed restoring deleted users and assigning them as managers in the same user sync

                   When syncing users and position assignments, sync will now process the
                   position assignments after all user creation/updates/deletion.

    T-13462        Fixed Program assignments based on the position start event.

                   This fixes a bug that was introduced recently. To fix affected programs
                   (those with "Completion time unknown" exceptions, where the students have
                   valid positions and start times), click "Save changes" in the assignments
                   tab of each affected program.

    T-13410        Link appraisal review question statuses

                   When the status of an item (e.g. a goal or competency) is changed, other
                   instances of the same item on the same page will automatically change to
                   match.

    T-13334        Fixed error when trying to send multiple Program Due messages in certifications

                   Some messages were being sent more than once or not at all while running
                   the cron.

    T-13457        Fixed incorrect enrolment start dates when uploading course completion records

                   When course completion records were uploaded using the coursecompletion
                   import tool, any user enrolment records created had a random start date in
                   2000. The enrolment start date is now the date of the course completion
                   import upload.

    T-13406        Ensured url property is set on reports before displaying report list
    T-13278        Fixed logic used when getting nested audiences
    T-13449        Fixed missing username fields on manager reservation pages
    T-13358        Fixed program extension requests which had been approved being overriden by cron
    T-13404        Fixed evidence filter not working in the Evidence report source
    T-13448        Fixed viewframeworks capabilities default values
    T-13394        Fixed coding error in course catalog for unenrolled users
    T-13400        Fixed Program Completion report source displaying deleted programs
    T-13429        Fixed broken custom logo in Kiwifruit responsive theme
    T-13423        Fixed border going off the page in A4 formats in certificates
    T-13367        Show error rather than warning when accessing hidden course

                   This prevents users from seeing the names of courses which should be hidden
                   to them, preventing privacy issues.


Release 2.6.12 (21st October 2014):
==================================================

Improvements:
    T-12823        Removed invalid string in language pack for the totara_core component
    T-12610        Improved multilingual support in mod_quiz question bank category selector

Bug Fixes:
    T-13285        Fixed sorting by 'Certification Due Date' in report source RoL Certifications
    T-12640        Fixed issue with many text fields in large appraisals and feedback360

                   Added support for large tables when using MySQL 5.5 or later. Database
                   servers need to be configured to use the new Barracuda database format and
                   one file per table setting. Existing database tables are not fixed during
                   upgrade, use admin/cli/mysql_compressed_rows.php to fix existing
                   installations.

                   PostgreSQL and MS SQL servers are not affected.

    T-13300        Fixed course form visibility field for roles based on "Editing trainer" archetype
    T-10554        Fixed reportbuilder reports for old (2.2) and new Assignment modules
    T-13297        Fixed the Program is live warning while editing Programs when audience visibility is enabled
    T-11938        Fixed warning in the program assignment tab when using position assignment type

                   This happens when using the position assignment type and several users
                   don't have primary position assigned.

    T-12300        Fixed Face-to-face session visibility not working as expected in Face-to-face calendar when audience visibility is enabled
    T-13101        Fixed dropdown widths in older browsers
    T-12765        Added sort order in program assignments to select the completion date for a user

                   IMPACT: when having more than one assignment for a user in programs or
                   certifications, it is not possible to know what completion date is taken
                   into account in the user's due date. Now a sort order has been added, so
                   the last assignment made for the user is the one the system uses to
                   determine the due date.

                   Also, there is a change that allows overriding the overdue date in
                   certifications once it has passed. So, if a certification has expired for a
                   user and the completion date set is greater than the expiry date, then it
                   will be used as the new expiry date.

    T-13361        Fixed due date not being updated in program messages when an exception is resolved
    T-13281        Allow site administrators to edit files in single activity format courses when display is set to open or download
    T-13318        Fixed issue where uploading course completion records can result in incorrect Program completion dates

                   IMPACT: if the course completion upload tool is used to upload course
                   completions, and the completed course then results in a program being
                   completed, the resulting program completion dates are incorrect as the code
                   incorrectly uses the MAX completion date in the uploaded CSV to set the
                   completion date for every user in the Program.

                   SCOPE: every user in a program that contains courses, where completion data
                   is being uploaded via the course completion tool


API Changes:
    T-12906        Fixed links to embedded reports in My Reports

                   Added new public property 'url' to every $report object

Release 2.6.11 (7th October 2014):
==================================================

Database Upgrades:
    T-13169        Fixed incorrect column type string in Record Of Learning Certifications report

Bug Fixes:
    T-13199        Fixed the handling of program exceptions missing the linked user assignment
    T-12878        Fixed course enrolment checks to take audience visiblity into account
    T-13278        Fixed nested audiences being updated even when they are unavailable
    T-13054        Fixed overdue warning incorrectly appearing on completed programs and certifications
    T-12660        Fixed capability checks for Audience dialog on course settings page
    T-13102        Fixed border on totara/custom menu drop downs on older browsers
    T-13282        Fixed managing courses and categories page on older browsers
    T-13229        Fixed RPL course completion records being deleted when an activities settings are updated
    T-12942        Fixed display issue in appraisals where visible to overlaps entry field
    T-13137        Fixed capability checks in course visibility for current user
    T-12987        Fixed overwriting a users existing 'Auth' field with totara sync
    T-13118        Removed the docking block functionality on older browsers
    T-13056        Fixed the ordering of html tags on the user profile page when a field is empty
    T-13046        Fixed bulleted and numbered lists in TinyMCE editors for RTL languages
    T-13197        Fixed certification link in ROL pointing to required learning
    T-13178        Fixed column grade not showing in courses report when uploading course completion records
    T-13289        Prevent errors in calendar from Facetoface with sessions with the same start time
    T-12486        Fixed the focus for text boxes in modal dialogs
    T-13196        Changed reportbuilder capability check to use the user:viewalldetails capability
    T-13174        Fixed fatal error on Record Of Learning : Certifications report source when user content restrictions are enabled
    T-13194        Fixed uniqueness of param keys for audience rules
    T-12682        Fixed reportbuilder export to PDF compatibility with IOS devices.
    T-13205        Fixed Program exceptions count methods incorrectly including deleted users


Release 2.6.10 (23rd September 2014):
==================================================

Security issues:
    MoodleHQ       http://docs.moodle.org/dev/Moodle_2.6.5_release_notes
    T-12620        Fixed Facetoface access control issues

Improvements:
    T-13017        Added help button to body field in Facetoface notifications to explain placeholders
    T-12475        Improved Program enrolment message for Single Activity format course
    T-12606        Added a default user email address setting to Totara Sync

Bug Fixes:
    T-12140        Fixed undefined offset errors on the SCORM Interaction report
    T-12748        Fixed temporary manager restriction default value
    T-12848        Fixed course availability through programs after program expires
    T-12977        Fixed Facetoface notification emails when there are scheduling conflicts
    T-12980        Fixed allowing upper case values in auth field in Totara Sync
    T-12973        Fixed alignment of the user table in right-to-left languages
    T-12481        Fixed format for custom user profile fields in bulk user actions download
    T-13155        Fixed launch of SCORMs using simple popup display mode in certain languages
    T-13107        Fixed sending of Facetoface notification emails to cancelled users
    T-13148        Fixed scalability of appraisals and Facetoface upgrade script for large numbers of deleted users
    T-13128        Fixed multilang support when menu and multiselect course custom fields are used as reportbuilder filters
    T-13054        Fixed incorrect overdue status being displayed on completed programs and certifications
    T-13007        Fixed creation of dynamic audience rules with empty parameters
    T-13085        Fixed formatting of course details section when managing courses and categories in older browsers
    T-13082        Fixed formatting help icons in middle of text for older browsers
    T-13005        Fixed recording which user has booked other users to a Facetoface session
    T-13130        Fixed exception when updating course categories with an empty ID Number
    T-13063        Removed excess obsolete entries from cohort rule table
    T-13081        Fixed Totara Sync failure when deleting users if duplicate ID Numbers exist


Release 2.6.9 (9th September 2014):
==================================================

Improvements:
    T-13001        Improved multilingual support for View Activity completion criteria

Bug Fixes:
    T-12872        Fixed appraisal stages so they can be completed on the final due day
    T-12978        Fixed label field in Feedback items to accept non-English characters
    T-12975        Fixed MSSQL error when viewing courses with linked evidence in a Learning Plan
    T-12669        Fixed ability of manager to add a previously-declined user to a Facetoface session
    T-13024        Fixed reportbuilder strings to meet AMOS requirements
    T-12985        Fixed setting of default user for course badge creators when restoring a course backup
    T-13010        Fixed the user join in Facetoface attendance exports
    T-12940        Fixed behaviour of Recipient Groups and Recipients when sending messages to users on a Facetoface session
    T-12988        Fixed Totara Sync incorrectly deleting existing users when the CSV source has invalid values
    T-12906        Fixed links to embedded reports in My Reports
    T-13014        Fixed missing type strings for translation in Facetoface session summary report source
    T-12893        Fixed the TinyMCE Editor fullscreen mode on IE8
    T-12989        Fixed reportbuilder caching for reports containing columns with incorrectly formatted date/time data
    T-12888        Fixed display of dates for Facetoface sessions on the Upcoming Events block
    T-12976        Fixed position of labels in right to left languages
    T-12946        Fixed the "Unlock and delete existing completion data" button to ensure criteria are unlocked
    T-12965        Fixed schema differences on upgrade from 2.5 to 2.6 with MSSQL


Release 2.6.8 (26th August 2014):
==================================================

Improvements:
    T-12943        Improved debugging on Audience rules tab

Database Upgrades:
    T-12581        Fixed database differences when upgrading from Totara 2.2 to 2.6

Bug Fixes:
    T-12910        Fixed required parameter checks on the edit scheduled reports page
    T-12950        Fixed Report builder caching so it doesn't break on MSSQL
    T-12917        Fixed wording of breadcrumbs when viewing learner details as a manager
    T-12767        Fixed backup and restore of Facetoface sessions without rooms
    T-12912        Fixed display of linked courses after adding a course to a competency
    T-12635        Fixed installation recovery if installation of Totara MSSQL module fails
    T-12776        Fixed incorrect error message being shown in Totara sync when an invalid source is selected
    T-12880        Fixed database error when deleting Programs
    T-12163        Fixed hover state on some form buttons in responsive themes
    T-12725        Fixed filtering by dates before January 1970 in Reportbuilder
    T-12511        Fixed get_roles_involved sql query when previewing an appraisal
    T-12895        Fixed missing username fields when viewing pending face-to-face session approvals
    T-12890        Fixed Totara Sync to allow spaces in directory paths
    T-12202        Fixed incorrect modal behaviour in dialogs when help icons are selected
    T-12938        Fixed Reportbuilder upgrade error when using MySQL
    T-12391        Fixed focus order of controls in the Calendar
    T-12904        Fixed Totara Sync to allow @ in directory paths

API Changes:
    T-12713        Enforce unique property if set when importing user custom profile fields with Sync


Release 2.6.7 (12th August 2014):
==================================================

Improvements:
    T-12735        Improved scalability for the program management page
    T-12311        Added checks for Program availability to the Program catalog
    T-12785        Added capability check to hide facetoface session attendees add/remove dropdown depending on permissions
    T-12876        Added forced cache purge on every upgrade

Bug Fixes:
    T-12778        Fixed Face-to-face calendar prev/next month display
    T-12502        Fixed sidebar filters in enhanced catalog when not logged in
    T-12224        Fixed alignment of framework dropdown in totara dialogs in the standard reponsive theme
    T-12464        Added error messages when importing course completion records with different grade on same day, user and course
    T-12799        Fixed filtering of course name through multilang filter for Certificates
    T-12694        Stopped reservation info being shown on session list page if reservations are turned off
    T-12874        Stopped notices being displayed on attendees page if no attendees are selected when add dialog is submitted
    T-12845        Fixed hardcoded column/filter section headings in Reportbuilder
    T-12835        Fixed quiz activities sending blank messages
    T-12786        Fixed course completion not working properly when another course is a prerequisite for completion
    T-12872        Fixed Appraisal Stages so they can be completed on the final due day
    T-12775        Disabled incorrect trust text usage
    T-12768        Fixed incorrect capacity and places totals in the Facetoface Summary report source
    T-12824        Fixed Certification id being incorrectly set in creation event objects
    T-12829        Fixed error on cron when a certification does not contain any courses


Release 2.6.6 (29th July 2014):
==================================================

Security issues:
    T-12619        Improved sesskey checks throughout the system
    T-12745        Improved capability checks around Reportbuilder scheduled reports
    T-12634        Fixed an issue with file downloads in Feedback360 and Appraisals
    T-12633        Fixed an issue with the session data when viewing/downloading a Certificate
    T-12632        Fixed an issue with token access for external Feedback360 requests

Improvements:
    T-12677        Improved error messages for Totara Sync
    T-12693        Improved validation checks around retrieving Programs within a category
    T-12099        Improved developer debugging in Reportbuilder
    T-12771        Added a SVG icon for Facetoface activities
    T-12561        Increased the maximum length of Hierarchy scale names and values for use with the multi-lang filter

Bug Fixes:
    T-12487        Fixed the type of assignment set when uploading completion records for Certifications
    T-12780        Fixed the formatting of dates when viewing a Badge
    T-12761        Fixed an undefined timezone issue in Reportbuilder caching
    T-12668        Fixed Programs potentially appearing multiple times in a user's Required Learning
    T-12403        Fixed the empty duration label when creating new events
    T-12720        Fixed an issue with filtering messages by icon in the Alerts block
    T-12576        Fixed the handling of epoch date for Reportbuilder date/time filters
    T-12621        Fixed the creation of file attachments in Facetoface
    T-11556        Fixed resolving Program exceptions through setting a realistic time
    T-12730        Fixed missing strings in Customtotara and Customtotararesponsive Themes
    T-12515        Fixed parameter names for manager rules in Dynamic Audiences
    T-12445        Fixed URL encoding in Hierarchies
    T-12283        Fixed docking for the Kiwifruitresponsive Theme
    T-12284        Fixed docking for the Kiwifruit Theme
    T-12675        Fixed the ordering of completion criteria for course completion reports
    T-12742        Fixed the downloading of a Badge image
    T-12717        Fixed an issue with the My Team report when adding temporary reports
    T-12731        Fixed the hardcoded 'Participants' string in Appraisals
    T-12737        Fixed the description for the Enable Audience-based Visibility setting
    T-12760        Added the default database collation in all tables including temporary tables


Release 2.6.5 (16th July 2014):
==================================================

Security issues:
    MoodleHQ       http://docs.moodle.org/dev/Moodle_2.6.4_release_notes
    T-12579        Fixed potential security risk in Totara Sync when using database sources

Improvements:
    T-12497        Improved internationalisation for the display of audience rules
    T-12547        Added validity checks to the position assignments form
    T-12591        Backported MDL-45985 new database schema checking script from Moodle 2.8
    T-10684        Added checks to prevent downgrades from a higher version of Moodle

Bug Fixes:
    T-12521        Fixed dynamic audiences not updating if the cohort enrolment plugin is disabled
    T-12203        Fixed reaggregation of Competencies when the aggregation type is changed
    T-12672        Fixed Totara Sync deleting users with no idnumber set
    T-12658        Fixed capabilities of Site Manager to enable them to create hierarchy frameworks
    T-11447        Fixed error on upgrade from Moodle to Totara
    T-12691        Fixed the sending of Stage Due messages in the Appraisal cron
    T-12567        Fixed the starting of new attempts for completed SCORMs which open in a new window
    T-12676        Fixed Totara Sync database source connections with non-alphanumeric passwords
    T-12636        Fixed addition of user middle name columns in Reportbuilder sources
    T-12524        Fixed the default facetoface reminder notifications failing to send
    T-12674        Fixed error when a user tries to show/hide columns in an embedded report
    T-12678        Fixed errors when using Totara Sync with database sources when position dates are text fields
    T-12710        Fixed display of users with no email addresses
    T-12588        Fixed Excel exports failing on some versions of PHP
    T-12299        Fixed appearance of docks in RTL languages
    T-11883        Fixed the multilang filter for goal and competency scales
    T-12623        Fixed the "view all" link in the record of learning and required learning sidebar
    T-12324        Fixed the formatting of date fields in Excel exports
    T-12545        Fixed deletion of associated data when deleting a facetoface notification
    T-12657        Fixed the padding for the body element in Internet Explorer
    T-12489        Fixed an issue with expanding a SCORM activity from the navigation block in a course


Release 2.6.4 (1st July 2014):
==================================================

Improvements:
    T-12605        Added logic to serve older versions of jquery to older versions of IE
    T-12497        Improved internationalisation for the display of audience rules
    T-12527        Added username of creator to Facetoface report and improved logging of attendees actions

Database Upgrades:
    T-12578        Added the ability to continue appraisals with missing roles
    T-11887        Fixed display of appraisals after a user has been deleted

Bug Fixes:
    T-12521        Fixed dynamic audiences not updating if the cohort enrolment plugin is disabled
    T-12538        Fixed category drop down selector not working correctly when creating programs
    T-12570        Fixed the sending of Program messages when completion is set relative to an action
    T-12479        Fixed the activate link incorrectly showing while viewing closed feedback360
    T-12509        Fixed historical course completion records not showing on the my team tab
    T-12563        Changed the default "temporary manager restrict selection" setting to "all users" for new installs
    T-12572        Added check to ensure generator columns can not be added to the same report multiple times
    T-12498        Fixed the display of custom field names for audience rules
    T-12156        Fixed cancellation message when F2F activity email notifications are turned off
    T-12571        Fixed the view hidden courses capability in the enhanced catalog
    T-12488        Fixed dynamic audiences showing on the 'add to audiences' option in bulk user actions
    T-12465        Fixed duplicate records issue when importing more than 250 course completion records
    T-12500        Fixed the incorrect use of urldecode function on page parameters
    T-12372        Fixed learning plan comments linking to the wrong components
    T-12387        Fixed the page title for program/certification searches
    T-12531        Fixed the formatting of the heading for facetoface attendance exports


Release 2.6.3 (17th June 2014):
==================================================

Database Upgrades:
    T-12541    Removed unused categoryid column from table prog_info_field
    T-12034    Fixed sending of Facetoface notifications where messages were not sent to every user in a session

Improvements:
    T-12466    Added 'Asia/Kolkota' lang string to timezone language pack
    T-12385    Added content filter to user reports to allow temporary managers to see their staff
    T-12530    Added room filter to Facetoface session view page
    T-12544    Added admin page to check current role capabilities against the installation defaults
    T-12494    Added ability to edit/delete evidence items created through course completion upload - Requires role with totara/plan:accessanyplan or totara/plan:editsiteevidence capabilities

Bug Fixes:
    T-12303    Fixed duplicated text on upgrade screen
    T-12431    Fixed setup of Totara-specific roles on new installs
    T-12263    Fixed Audience Visible Learning tab type selector
    T-12510    Fixed Audience language strings where cohort was still being used
    T-12162    Fixed custom fields from being both required and locked
    T-12534    Fixed sending of duplicated notifications without variable substitution in Program messages
    T-12491    Fixed Program Overview report to show correct Manager info
    T-12097    Fixed behaviour of Program content tab when javascript is disabled
    T-12519    Fixed certification pagination wrongly linking to programs
    T-12480    Fixed assigning of incorrect course IDs when approving a Learning Plan competency linked to a course
    T-12505    Fixed alignment of navigation elements in RTL languages in Kiwifruitresponsive theme
    T-12493    Fixed display of menu in RTL languages in Standardtotararesponsive theme
    T-12506    Fixed RTL arrow image on My Learning page in Kiwifruitresponsive theme
    T-12501    Fixed deprecated function warning when closing an Appraisal
    T-12513    Fixed display of Appraisal status code in Appraisal Summary report
    T-12512    Fixed column options in Appraisal Details report
    T-12492    Fixed Record of Learning Evidence report when using "show records based on users" option
    T-12526    Fixed PHP undefined property error in Record of Learning Evidence report
    T-12242    Fixed file saving on scheduled reports when "Export to filesystem" is disabled at site level
    T-12525    Fixed errors with Facetoface attendance report export to CSV
    T-12320    Fixed Facetoface iCal attachment line breaks in long descriptions
    T-11816    Fixed display of Articulate Storyline SCORMS in iPads - use new display setting of "New Window (simple)"


Release 2.6.2 (3rd June 2014):
==================================================

Security Fixes:
    T-12441    Fixed potential XSS vulnerability in quicklinks block

Improvements:
    T-11961    Added ability to assign Audience members based on position & organisation types
    T-12326    Extended execution time on completion reaggregation script
    T-12483    Added new alternate name fields when importing users with totara_sync
    T-12364    Improved contrast on Hierarchy selected items to meet Accessibility guidelines

Bug Fixes:
    T-12467    Fixed display of SCORM packages on secure HTTPS sites
    T-12463    Fixed critical SCORM error where subsequent attempts after an initial failed attempt are not recorded
    T-12471    Fixed display of grades in Course Completion Report for grades uploaded by completion import tool
    T-12444    Fixed course completion import report sometimes returning zero records
    T-12469    Fixed sending of notifications when a Facetoface booking date/time is changed
    T-12277    Fixed Face-to-face reminders still being sent to users who have cancelled from a session
    T-12121    Fixed transaction error when quiz completion triggers sending of messages
    T-12307    Fixed days not being translated in weekly scheduled reports
    T-12327    Fixed issue with dialog boxes being too wide for some screens
    T-12179    Fixed choosing of position on email self-registration when Javascript is disabled
    T-12263    Fixed Javascript for type filter dropdown in Audience Visibility
    T-12451    Fixed sort order of dependent courses in Course Completion settings
    T-12461    Fixed display of move and settings admin options for Quicklinks block
    T-12184    Fixed capitalisation of Program and Certification columns in Course Catalog
    T-12455    Fixed changing of visibility of a Certification on Audience Visible Learning tab
    T-12368    Fixed hidden labels in Hierarchy search dialog
    T-12371    Fixed alt attribute on course icons
    T-12362    Fixed alt and title attributes on competency icons
    T-12376    Fixed labels when creating a scheduled report in ReportBuilder
    T-12379    Fixed page title when deleting scheduled report
    T-12349    Fixed page title when deleting a Learning Plan
    T-12348    Fixed table column header on list of Learning Plans
    T-12237    Fixed HTML table in Alerts information popup dialog
    T-12473    Removed redundant get_totara_menu function in totara_core
    T-12478    Removed blink tag from element library


Release 2.6.1 (20th May 2014):
==================================================

Security Fixes:
    MoodleHQ    http://docs.moodle.org/dev/Moodle_2.6.3_release_notes

Improvements:
    T-12195    Improved error handling in F2F bulk add attendees
    T-12238    The alerts block is now a list instead of a table
    T-12313    Removed request approval button in Learning Plans while request is pending
    T-12375    Improved accessibility by combining links under My Reports
    T-12399    Improved look of the events filter on the calendar page
    T-12433    Show participants in appraisal overview page and pdf snapshots
    T-12201    Improved clarity of Audience Visibility language strings

Bug Fixes:
    T-12307    Fixed days not being translated in weekly scheduled reports
    T-12306    Added styling back into the program assignments page
    T-12017    Fixed alternate name fields for external badges
    T-12017    Fixed alternate name fields for trainer roles in face to face
    T-12017    Fixed alternate name fields on manager rules
    T-12234    Fixed highlight effect on Kiwifruit themes
    T-12446    Fixed display issue where save search button was overlaying column headers
    T-12326    Recover activity completion, grade and previous course completion data
    T-12246    Fixed course completion data reset for all users when a course is used as content in a certification
    T-12314    Fixed unknown column error when creating a program with multi_select custom field
    T-12434    The search and clear button on the find courses now are hidden immediately
    T-12278    Fixed facetoface attendance export not showing data if a users do not have a manager assigned to them
    T-12254    Fixed sort order of Facetoface attendees and requested users in Feedback360
    T-12248    Fixed SCORM redirect when it is opened in a new window
    T-12318    Fixed issue where custom field menus did not work as expected in responsive themes
    T-12310    Fixed display of custom field images in the enhanced catalog
    T-12153    Fixed the setting of users timecreated field when new users are created by Totara Sync
    T-12160    Fixed breadcrumbs when viewing staffs record of learning
    T-12204    Fixed incorrect error message being displayed when uploading huge files


Release 2.6.0.1 (7th May 2014):
==================================================

Bug Fixes:

    T-12880    Fix critical error causing deletion of course completion criteria data
    T-12149    Fix navigation menu when adding course custom fields


Release 2.6.0 (5th May 2014):
==================================================

New features:

T-7865    Allow recursive searches down the management hierarchy.
T-8592    Option to allow users to select their own organisation/position/manager during self-registration.
T-9736    Improve saved search interface.
T-9783    Allow manager to add a reason when declining/accepting learning plan and program extension requests.
T-10226   New report source for displaying face to face session information.
T-10239   Additional variables available in program messages.
T-10347   Relative date support for dynamic audience course/program completion rules.
T-10850   Ability to turn off face to face notifications at the site level.
T-10914   Ability for administrators to disable or hide certain functionality.
T-11067   Ability to assign system roles to all members of an audience.
T-11112   Totara sync now supports importing the 'emailstop' field.
T-11497   Ability to upload custom course/program icons.
T-11593   Enhanced Catalog with faceted search.
T-11593   Program custom fields now available.
T-11593   Report builder now supports sidebar filters, automatic results reloading and simple toolbar search options.
T-11593   New multi-select custom field type for hierarchy and course custom fields.
T-11597   Ability to mark face to face attendance in bulk.
T-11722   Organisation and position content restrictions added to appraisal reports.
T-11741   Managers can now reserve spaces in face to face sessions without naming the attendees. Thanks to Xtractor and Synergy Learning.
T-11752   New session start and end filters for the face to face sessions report source.
T-11879   Ability to force password changes for new users in Totara sync.
T-11988   Add report builder support to enrolment plugins. Thanks to Phil Lello from Catalyst EU.
T-11999   Add report builder embedded report support to plugins. Thanks to Phil Lello from Catalyst EU.
T-12109   Add links to completed stages on appraisal summary page.


2.6 Database schema changes:
============================

New tables:

Bug ID      New table name
--------------------------
T-11067     cohort_role
T-11593     course_info_data_param
T-11593     comp_info_data_param
T-11593     pos_info_data_param
T-11593     org_info_data_param
T-11593     goal_info_data_param
T-11593     prog_info_field
T-11593     prog_info_data
T-11593     prog_info_data_param
T-11593     report_builder_search_cols

New fields:

Bug ID      Table name                  New field name
------------------------------------------------------
T-9783      dp_plan_history             reasonfordecision
T-9783      dp_plan_competency_assign   reasonfordecision
T-9783      dp_plan_course_assign       reasonfordecision
T-9783      dp_plan_program_assign      reasonfordecision
T-9783      dp_plan_objective_assign    reasonfordecision
T-9783      prog_extension              reasonfordecision
T-11593     report_builder              toolbarsearch
T-11593     report_builder_filters      region

Other database changes:

T-11166     Report builder exportoptions converted from bitwise to comma separated list.
T-7865      Report builder settings updated: 'user_content', 'who' value switched from string to bitwise integer constant.
T-10914     Totara advanced feature settings migrated to new format.
T-11593     MSSQL group concat extension added. Due to requirement to install group concat plugin, MSSQL DB user requires additional permissions during install/upgrade: ALTER SETTINGS(SERVER)


2.6 API Changes:
================

== Enhanced catalog (T-11593) ==

* display_table() should now be always called, even if there are no rows in the results. This
  function will display a message if there are no rows to display. Remove "if ($countfiltered>0)"
  from embedded pages. This was done because the toolbar search is built into the display table
  header.

* Capability checks should be moved from embedded pages to is_capable() function in embedded
  classes. This function is called during the report constructor of embedded reports. If the
  is_capable method is not implemented then report builder assumes that the capabilities have
  not yet been recoded and will disable instant filters (instant filters go directly to the
  embedded class and bypass the embedded page, which is why the capability checks had to be moved).
  is_capable is passed the report object which can be used to access params, if required.

* rb_filter_type constructor and get_filter have been changed to include a region parameter. If any
  custom filter types have been added which define their own constructor method then they need to
  be updated to accept the additional parameter and pass it to the parent constructor. Any call
  get_filter must be updated (there are unlikely to be any custom calls to get_filter).

* get_extrabuttons() is a new function for embedded report sources that lets you specify a button or
  buttons to go in the top right of the table's toolbar. Simply override the inherited function in
  the desired report source and make it return the rendered output of any buttons you want to add.
  See the embedded catalog report sources for an example.

== Indirect reports patch (T-7865) ==

* The rb_content_option constructor method now accepts either a string or an array for the 3rd
  argument (previously it was just a string). The argument in
  totara/reportbuilder/classes/rb_content_option.php has changed from $field to $fields.

* To maintain backward compatibility, content options will still work with strings, so any custom
  content restrictions _do not_ need to be updated.

* However, the 'user' content option has been updated to pass additional information so any report
  sources that use the 'user' content option need to update the code.

Previously the code would look something like this:

             new rb_content_option(
                 'user',
                 get_string('users'),
                 '[TABLENAME].[FIELDNAME]'
             ),

Whereas now it must look like this:

             new rb_content_option(
                 'user',
                 get_string('users'),
                 array(
                     'userid' => '[TABLENAME].[FIELDNAME]',
                     'managerid' => 'position_assignment.managerid',
                     'managerpath' => 'position_assignment.managerpath',
                     'postype' => 'position_assignment.type',
                 ),
                 'position_assignment'
             ),

Where [TABLENAME] and [FIELDNAME] are typically something like 'base' and 'userid'.

The two key changes are the 3rd argument (where the string is replaced with the array with extra
data), and the 4th argument (where 'position_assignment' is added as a join). In the example above
there were no other joins (the 4th argument was empty). If there are already one or more join
options you will need to convert the 4th argument to an array and add 'position_assignment'. So if
the fourth argument was this:

'dp'

You would need to update to be:

array('dp', 'position_assignment')

Finally you need to make sure that the 'position_assignment' join is available. This can be done
with a line like this:

 $this->add_position_tables_to_joinlist($joinlist, 'base', 'userid');

in the define_joinlist() method. The 2nd and third arguments should reference a table and field used
above for [TABLENAME] and [FIELDNAME].


== Changes to Totara email user function (T-12077) ==

The function totara_generate_email_user() is now deprecated. Update references to use:
\totara_core\totara_user::get_external_user() instead.

== Deprecation of 'standardtotara' theme ==

In Totara 2.6 the 'standardtotara' theme is deprecated in favour of 'standardtotararesponsive'.
'standardtotara' is still present in 2.6 but will be removed in 2.7.

See this guide for how to migrate your 2.5 theme to 2.6:

http://community.totaralms.com/mod/resource/view.php?id=1869

== MSSQL only ==

Now require additional permissions to install:

MSSQL DB user required additional permissions: ALTER SETTINGS(SERVER)

This is due to requirement to install group concat plugin.

*/
?>
