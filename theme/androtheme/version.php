<?php
/**
* Theme version info
*
* @package    theme
* @subpackage androtheme
* @copyright  Androgogic Pty Ltd
*
* Documentation: http://docs.moodle.org/dev/version.php
*/

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015073100; // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013050100; // Requires this Moodle version
$plugin->component = 'theme_androtheme'; // Type and name of this plugin
$plugin->release = 'v1.6';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2013110500,
);