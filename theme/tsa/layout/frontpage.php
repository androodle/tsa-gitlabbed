<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Layout - frontpage
 *
 * @package   theme_androtheme
 * @author Androgogic <support@androgogic.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 include(__DIR__.'/includes/header.php'); ?>

<div id="page" class="container-fluid">
    <div id="page-content" class="row-fluid">
        <div id="region-bs-main-and-pre" class="span9 <?php echo $left ? "pull-left" : "pull-right" ?>">
            <div class="row-fluid">
                <section id="region-main" class="span8 <?php echo $left ? "pull-right" : "pull-left" ?>">
                    <?php echo $OUTPUT->course_content_header(); ?>
                    
                    <!-- CUSTOM HOME START -->
                    <div class="region-content" id="custom-homepage-regions">
                        <?php include(__DIR__.'/includes/homepage.php'); ?>
                    </div>
                    <!-- CUSTOM HOME END -->
                    
                    <?php echo $OUTPUT->main_content(); ?>
                    <?php echo $OUTPUT->course_content_footer(); ?>
                </section>
                <?php echo $OUTPUT->blocks('side-pre', 'span4' . ($left ? ' desktop-first-column' : '')); ?>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', 'span3' . (!$left ? ' desktop-first-column pull-left' : '')); ?>
    </div>
</div>    

<?php include(__DIR__.'/includes/footer.php'); ?>
