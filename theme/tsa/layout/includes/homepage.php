<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Layout - homepage
 *
 * @package   theme_androtheme
 * @author Androgogic <support@androgogic.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
?>

<div id="region-hero-unit">
    <?php
        echo $OUTPUT->heading(get_string('welcome-text', 'theme_androtheme', $USER->firstname), 1, 'hero-heading');
        echo $OUTPUT->blocks('hero-unit');
    ?>
</div>

<div id="region-two-cols-wrap" class="clearfix">
    <div class="column"><?php echo $OUTPUT->blocks('two-cols-left'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('two-cols-right'); ?></div>
</div>

<div id="region-three-cols-wrap" class="clearfix">
    <div class="three-cols-header"><?php echo $OUTPUT->blocks('three-cols-head'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('three-cols-left'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('three-cols-mid'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('three-cols-right'); ?></div>
</div>

<div id="region-four-cols-wrap" class="clearfix">
    <div class="four-cols-header"><?php echo $OUTPUT->blocks('four-cols-head'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-lefta'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-leftb'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-righta'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-rightb'); ?></div>
</div>