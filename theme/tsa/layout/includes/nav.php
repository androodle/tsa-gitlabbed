<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Layout - nav
 *
 * @package   theme_androtheme
 * @author Androgogic <support@androgogic.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
?>

<header role="banner" class="navbar navbar-fixed-top moodle-has-zindex">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="<?php echo $CFG->wwwroot;?>"><?php echo $SITE->shortname; ?></a>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
                <?php if ($showmenu) { ?>
                    <?php if ($hascustommenu) { ?>
                    <div id="custommenu"><?php echo $custommenu; ?></div>
                    <?php } else { ?>
                    <div id="totaramenu"><?php echo $totaramenu; ?></div>
                    <?php } ?>
                <?php } ?>
                <ul class="nav pull-right">
                    <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    <li class="navbar-text">
                        <?php echo $OUTPUT->login_info() ?> 
                    </li>
                    <li class="user-picture"><?php if (isloggedin()) {echo $OUTPUT->user_picture($USER, array('size'=>20));}?></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="page-header" class="clearfix">
        <div id="page-header-wrapper" class="container-fluid">
            <div class="logo">
                <a href="<?php echo $CFG->wwwroot; ?>"> <img src="<?php echo $logourl; ?>" alt="<?php echo 'Link to ', $SITE->fullname, ' homepage';?>" title="<?php echo 'Link to ', $SITE->fullname, ' homepage';?>"/></a>
            </div>
            <div class="logounit">
                <img src="<?php echo $logouniturl; ?>" alt="" title=""/>
            </div>
            <?php echo $html->bannertext; ?>
        </div>
        
        <div id="page-navbar-wrapper" class="clearfix">    
            <div id="page-navbar" class="container-fluid">
                <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
            </div>
            <!--<?php echo $OUTPUT->page_heading(); ?>
            <div id="course-header">
                <?php echo $OUTPUT->course_header(); ?>
            </div>-->
        </div>
    </div>
</header>