<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Layout - footer
 *
 * @package   theme_androtheme
 * @author Androgogic <support@androgogic.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$hasfooter = (!isset($PAGE->layout_options['nofooter']) || !$PAGE->layout_options['nofooter'] );
$hasnavbar = (!isset($PAGE->layout_options['nonavbar']) || !$PAGE->layout_options['nonavbar'] );

if ($hasfooter) { ?>
<footer id="page-footer" class="row-fluid">
    <div id="footnotewrapper" class="container-fluid">
        <div class="footer-text">
            <span class="year">&copy; <?php echo date('Y')?></span>
            <?php echo$html->footnote; ?>
        </div>
        <div class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></div>
        <div class="footer-logo">
            <img src="<?php echo $OUTPUT->pix_url('bg_footer', 'theme')?>" alt="Unleashing Potential" />
        </div>
    </div> 
</footer>
<?php } ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>

</body>
</html>